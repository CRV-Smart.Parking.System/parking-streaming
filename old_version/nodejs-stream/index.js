// Se necesita correr startStreaming.bat de https://gitlab.com/crvsmartparkingsystem/parking-streaming
var nameStream = process.argv[2]; // Name of Stream
var streamURL = process.argv[3]; // Can be rtsp://localhost:8554/videoGrabado/2
var exposePort = process.argv[4]; // Port where listen clients from Front End. Can be 9999
Stream = require('node-rtsp-stream')
stream = new Stream({
  name: nameStream,
  streamUrl: streamURL, 
  wsPort: exposePort,
  ffmpegOptions: { // options ffmpeg flags
    '-max_muxing_queue_size': '9999',
    '-crf': '0',
    '-stats': '' // an option with no neccessary value uses a blank string
    //'-r': 60 // options with required values specify the value after the key
  }
})