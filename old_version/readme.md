# CRV Smart Parking System - Streaming SERVER

# Requisitos
- Servidor RTSP: RTSP Simple SERVER
- Emisor de fuente de video (Video mp4, retransmision, webcam, imagenes) ffmpeg y/o GStreamer
- Instalar ambos con las variables de entorno de windows

# Documentacion

## 0) Leer documentación oficial de RTSP SIMPLE SERVER
- https://github.com/aler9/rtsp-simple-server

## 1) Leer documentación oficial de ffmpeg
- https://ffmpeg.org/documentation.html
- https://news.mistserver.org/news/71/An+introduction+to+encoding+and+pushing+with+ffmpeg

## 1 bis) Leer documentación oficial de GStreamer
- https://gstreamer.freedesktop.org/documentation

# Instalar Requisitos

## 2) Descargar RTSP Simple Server v0.10.1
- https://github.com/aler9/rtsp-simple-server/releases/tag/v0.10.1
- O copiar la carpeta rtsp-simple-server a C:\ (C:\rtsp-simple-server)
- Asociar la carpeta con la variable de entorno PATH
- Crear una variable de entorno nueva llamada RTSP_SIMPLE_SERVER asociada con la carpeta mencionada

## 3) Cambiar config en rtsp-simple-server.yml 
El que viene por defecto 8000 estara ocupado por Django
- Cambiar puerto rtpPort  a 9000
- Cambiar puerto rtcpPort a 9001

## 4) Descargar ffmpeg
- HomePage https://ffmpeg.org/download.html
- Linux https://ffmpeg.org/download.html#build-linux
- MAC https://evermeet.cx/ffmpeg/ 
- Windows https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-full.7z
- Luego extraer la carpeta en alguna ubicación del disco (Ejemplo C:\ffmpeg)
- Asociar la carpeta bin con la variable de entorno PATH (Es decir C:\ffmpeg\bin)
- Tutorial: https://www.youtube.com/watch?v=DCJWJbRIiVA
- Tutorial2: http://4youngpadawans.com/stream-camera-video-and-audio-with-ffmpeg/

## 4bis) Descargar GStreamer
- Descarga: https://gstreamer.freedesktop.org/download
- Luego de instalar el programa asociar la carpeta bin con la variable de entorno PATH (Revisar donde se instalo GStreamer)
- Tutorial: https://www.youtube.com/watch?v=DCJWJbRIiVA

# Iniciar Streaming

# 5) Iniciar Server RTSP
- Ejecutar en la terminal "rtsp-simple-server %RTSP_SIMPLE_SERVER%\rtsp-simple-server.yml"
- %RTSP_SIMPLE_SERVER% es la variable de entorno que creamos anteriormente en el paso 2
- Esto debe hacerse asi, ya que si no se especifica el .yml de configuracion, por defecto nos lanza el server en el puerto 8000 y 8001 (Cosa que no queremos)

# Emitir Fuente de Video desde archivo

## 6) Hostear archivo de video con ffmpeg
- ffmpeg -re -stream_loop -1 -i video.mp4 -c copy -f rtsp rtsp://localhost:8554/mystream
- Sobre la carpeta donde este instalado ffmpeg
- Teniendo un archivo video.mp4 alojado en esa carpeta

## 6 bis) Hostear archivo video con GStreamer
- gst-launch-1.0 filesrc location=video.mp4 ! qtdemux ! rtspclientsink location=rtsp://localhost:8554/mystream

# Emitir fuente de video desde Webcam

## 7) Emitir webcam desde ffmpeg
- ffmpeg -f dshow -i video="Integrated Camera" -f rtsp rtsp://localhost:8554/mystream

# Visualizacion del Streaming

## 8) Ver Streaming 
En VLC
- Medio -> Abrir Ubicación de red -> rtsp://localhost:8554/mystream
- O ejecutar "vlc rtsp://localhost:8554/mystream"
En GStreamer
- gst-launch-1.0 rtspsrc location=rtsp://localhost:8554/mystream ! rtph264depay ! decodebin ! autovideosink
En FFmpeg
- ffmpeg -i rtsp://localhost:8554/mystream -c copy output.mp4

# Iniciar server automaticamente
- Correrr startStreaming.bat

